var path = require('path');
var express = require('express');
var app = express(); //invoke

app.set('view engine', 'jade'); //view engine -> name of library === jade
app.set('views', process.argv[3] || path.join(__dirname, 'views'));    // view -> folder passed to us

app.get('/home', function(req, res) {
    res.render('index', { date: new Date().toDateString() });
})

app.listen(process.argv[2] || 3000);