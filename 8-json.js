var express = require('express');
var app = express();
var fs = require('fs');
var path = require('path');

app.get('/books', function(req, res){
    var filename = process.argv[3] || './public/test.txt';

    fs.readFile(filename, function(e, data){
        if(e) return res.sendStatus(500);

        try {
            var books = JSON.parse(data)
        } catch(e) {
            res.sendStatus(500)
        }
        
        res.json( { data: books } );
    })
});

app.listen(process.argv[2] || 3000);