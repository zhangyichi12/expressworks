var express = require('express'); //Nodejs core module
var app = express();

app.get('*', function(req, res) {
  res.end('Hello World!');
});

app.listen(process.argv[2] || 3000);
