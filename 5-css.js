var path = require('path');
var express = require('express');
var app = express();

var file = path.join(__dirname, 'src');

//app.use(require('stylus').middleware(process.argv[3]) || path.join(__dirname, './src/main.styl'));
app.use(express.static(process.argv[3] || file));

app.listen(process.argv[2] || 3000);